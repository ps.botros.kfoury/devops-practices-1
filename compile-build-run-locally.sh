#compile
git_version=$(date -d @$(git log -n1 --format="%at") +%y%m%d)-$(git log -n1 --format="%h" --abbrev=8)
mvn versions:set -DnewVersion=$git_version
mvn clean package
#build
docker build -t botroskfoury/pdp:$git_version --build-arg AppVersion=$git_version -f docker/Dockerfile .
#run
docker run -it -e JAVA_OPTS='-Xmx1024m' -e DspringProfilesActive=h2 -e AppVersion=$git_version -p 8090:8090 botroskfoury/pdp:$git_version