git_release=$(date -d @$(git log -n1 --format="%at") +%y%m%d)-$CI_COMMIT_SHORT_SHA
helm package --app-version=$git_release ./kubernetes/helmpractice/pdpapp4
helm install pdpapp4 --debug ./pdpapp4-0.1.0.tgz -n $git_release --create-namespace