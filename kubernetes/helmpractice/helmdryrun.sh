git_release=$(date -d @$(git log -n1 --format="%at") +%y%m%d)-$(git log -n1 --format="%h" --abbrev=8)
helm package --app-version=$git_release pdpapp4
helm install $git_release --dry-run --debug ./pdpapp4-0.1.0.tgz --version=$git_version >a.txt