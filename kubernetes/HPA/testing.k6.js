import http from 'k6/http';
export default function () {
  for (var id = 1; id <= 10000; id++) {
    var url = 'https://example.com/persons/';
    var payload = JSON.stringify({
      name: 'Botros'+id,
      age: '37'
    });
  
    var params = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    http.post(url, payload, params);
  }
}